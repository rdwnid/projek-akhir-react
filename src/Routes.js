import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/login/Login";
import Search from "./components/Search";
import Nav from "./Nav";

export default function Routes() {
    return (
        <>
        <Router>
            <Switch>
                <Route exact path="/menu">
                    <Nav/>
                    Ada navigasi
                </Route>
                <Route path="/"> </Route>
            </Switch>
            <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>
                <Route exact path="/search">
                    <Search/>
                </Route>
                <Route exact path="/login">
                    <Login/>
                </Route>
            </Switch>
        </Router>
        </>
    )
}
