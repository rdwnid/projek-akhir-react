import React from "react";
import { BriefcaseIcon } from '@heroicons/react/solid';

export default function Hero() {
    return (
        <div className="my-5 px-6 py-6 bg-purple-700 dark:bg-gray-800 rounded-2xl md:py-12 md:px-12 lg:py-16 lg:px-16 xl:flex xl:items-center shadow-lg">
            <div className="xl:w-0 xl:flex-1">
                <BriefcaseIcon className="w-auto h-10 text-white inline"/>
                <h2 className="text-2xl leading-8 font-extrabold tracking-tight text-white sm:text-3xl sm:leading-9">
                Ada 31.778 lowongan untuk kamu
                </h2>
                <p className="mt-3 max-w-3xl text-lg leading-6 text-indigo-200">
                Telusuri lowongan kerja dan temukan kesempatan kariermu selanjutnya dengan situs rekrutmen ini.
                </p>
            </div>
            <div className="mt-8 sm:w-full sm:max-w-md xl:mt-0 xl:ml-8">
                <div className="mt-3 rounded-md shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
                <button className="w-full flex items-center justify-center px-5 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-purple-500 hover:bg-purple-400 focus:outline-none focus:bg-purple-400 transition duration-150 ease-in-out">
                    Download Now !
                </button>
                </div>
                <p className="mt-3 text-sm leading-5 text-indigo-200">
                Dapatkan notifikasi terbaru tentang lowongan pekerjaan yang sesuai dengan minat kamu hanya dengan mendownload aplikasinya.
                </p>
                <p className="mt-1 text-sm leading-5 text-indigo-200">
                Klik Download Now ! untuk mendapatkan aplikasinya. Tersedia untuk Android dan IOS.
                </p>
            </div>
        </div>
    )
}