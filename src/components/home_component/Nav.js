import React from "react";
import { SearchIcon, LoginIcon } from '@heroicons/react/outline';
import { Link } from "react-router-dom";

export default function Nav() {
    return (
    <header className="my-5 w-full shadow-lg bg-white dark:bg-gray-700 items-center h-16 rounded-2xl z-40">
        <div className="flex flex-col justify-center h-full px-3 mx-auto flex-center">
            <div className="justify-between items-center pl-1 flex w-full lg:max-w-68 sm:pr-2 sm:ml-0">
                <div className="basis-1/2 justify-between flex">
                <div className="flex basis-5/6 h-auto h-full">
                    <div className="relative flex items-center w-full h-full group">
                        <SearchIcon className="pointer-events-none absolute z-50 flex items-center justify-center block w-auto h-11 p-3 pr-2 text-sm font-extrabold text-gray-400 group-hover:text-gray-500 uppercase cursor-pointer"/>
                        <input type="text" className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-gray-800 text-gray-400 aa-input" placeholder="Search" />
                    </div>
                </div>
                <div className="basis-1/6 p-1 flex items-center justify-end sm:mr-0 sm:right-auto">
                    <Link to="/search" className="py-2 flex justify-center items-center  bg-purple-600 hover:bg-purple-700 focus:ring-purple-500 focus:ring-offset-purple-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
                        Cari
                    </Link>
                </div>
                </div>
                <div className="basis-1/12 p-1 flex items-center justify-end sm:mr-0 sm:right-auto">
                    <Link to="/login" className="py-2 flex justify-center items-center  bg-purple-600 hover:bg-purple-700 focus:ring-purple-500 focus:ring-offset-purple-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
                        <LoginIcon className="h-5 w-auto"/>
                        Login
                    </Link>
                </div>
            </div>
        </div>
    </header>
    )
}