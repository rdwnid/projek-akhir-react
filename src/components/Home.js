import React from "react";
import Content from "./home_component/Content";
import Footer from "./home_component/Footer";
import Hero from "./home_component/Hero";
import Nav from "./home_component/Nav";
import Pagination from "./home_component/Pagination";

export default function Home() {
    return (
        <div className="m-5">
            <Nav/>
            <Hero/>
            <Content/>
            <Pagination/>
            <Footer/>
        </div>
    )
}